<?php

namespace App\Http\Controllers;

use Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

/********************************************* *

 * Autor        : Shrikanth Kulkarni 
 * Email        : shrikanth.kulkarni@bridgera.com
 * Description  : 
 * Created for listening to WebHooks Triggered from Shopify Customer/user Actions.
 * Webhooks are configured in Admin Panel of Shopify -> Settings -> Notifications -> WebHooks.
 * 
 * 
******************************************************/

class shopifywebHookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /***************************************
     * @inParams   : Json Data Sent from shopify in response of Order Created by the user
     * @outParames :  none
     * This function is used to handle the Order Once Created in Shopify API 
     * 
     ************************************/
     

    public function manageOrder(Request $request){
      Log::info('Showing user: HELLO'.json_encode($request->json()->all())); 
      return $request->json()->all();
    }
    
}

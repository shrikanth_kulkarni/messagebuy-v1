<?php

namespace App\Http\Controllers;

use Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use PHPShopify\ShopifySDK\shopifyClient;

/********************************************* *

 * Autor        : Shrikanth Kulkarni 
 * Email        : shrikanth.kulkarni@bridgera.com
 * Description  : Created for Managing Orders by intercating with shopify API 
 * 
 * 
******************************************************/

class shopifyAPIController extends Controller
{
    public $_apiKey         =   '';
    public $_apiPassword    =   '';
    public $_storeName      =   '';
    public $_storeAPIurl    =   '';
    public $_protocolType   =   'https://';
 
    /***************************************
     * @inParams   : Json Data Sent from shopify in response of Order Created by the user
     * @outParames :  none
     * This function is used to handle the Order Once Created in Shopify API 
     * 
     ************************************/
    public function __construct()
    {
         $this->_apiKey         =   $_ENV['SHOPIFY_APIKEY'];
         $this->_apiPassword    =   $_ENV['SHOPIFY_APIPWD'];
         $this->_storeName      =   $_ENV['STORE_NAME'];
         $this->_storeAPIurl    =   $this->_protocolType.$this->_apiKey.':'.$this->_apiPassword.'@'. $this->_storeName;
    } 
     /* 
       @inParams  : Order ID 
       @outParams : Boolean

     */
    public function cancelOrder(Request $request){

        // $client = new \GuzzleHttp\Client();
        // $client->setDefaultOption('verify', false);
        // $Details=$client->post($this->_storeAPIurl.'/admin/orders/'.'1001'.'/'.'cancel.json');
        // return $Details;
        
        $shopifyClient = new \PHPShopify\ShopifySDK();
        $scopes = 'read_products,write_products,read_script_tags,write_script_tags';
        $config = array(
            'ShopUrl' => $_ENV['SHOP_URL'],
            'ApiKey' => $_ENV['SHOPIFY_APIKEY'],
            'Password' => $_ENV['SHOPIFY_APIPWD'],
        );
        $order = array (
            "order_id"=>'1004'
        );
        $shopifyClient->config($config);
        return $products = $shopifyClient->Order->cancel->post('1004');




    }

   



    
}
